#coding:utf-8
"""

Projet NSI: Le choixpeau magique : 1ère partie

@author: Mayline, Yakin, Yacine

--------------------------------------------------------------------------------------------------------------------------------------
PARTIE 1:
"""
import csv

with open("Characters.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    characters_tab = [{key : value.replace('\xa0', ' ') for key, value in element.items()} for element in reader]

with open("Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    characteristics_tab = [{key : value for key, value in element.items()} for element in reader]

poudlard_characters = []

for poudlard_character in characteristics_tab:
    for kaggle_character in characters_tab:
        if poudlard_character['Name'] == kaggle_character['Name']:
            poudlard_character.update(kaggle_character)
            poudlard_characters.append(poudlard_character)

index_id = {int(character['Id']): character for character in poudlard_characters}

index_id_characteristics = {int(character['Id']): 
                            (int(character['Courage']),
                             int(character['Ambition']),
                             int(character['Intelligence']),
                             int(character['Good'])) 
                            for character in poudlard_characters}

print(index_id_characteristics)