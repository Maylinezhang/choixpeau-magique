import csv
from math import sqrt

with open("Characters.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    characters_tab = [{key : value.replace('\xa0', ' ') for key, value in element.items()} for element in reader]

with open("Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    characteristics_tab = [{key : value for key, value in element.items()} for element in reader]

profiles_student = [{'Name': 'student1', 'Courage' : 9, 'Ambition' : 2, 'Intelligence' : 8, 'Good' : 9},
                    {'Name': 'student2', 'Courage' : 6, 'Ambition' : 7, 'Intelligence' : 9, 'Good' : 7},
                    {'Name': 'student3', 'Courage' : 3, 'Ambition' : 8, 'Intelligence' : 6, 'Good' : 3},
                    {'Name': 'student4', 'Courage' : 2, 'Ambition' : 3, 'Intelligence' : 7, 'Good' : 8},
                    {'Name': 'student5', 'Courage' : 3, 'Ambition' : 4, 'Intelligence' : 8, 'Good' : 8}]

poudlard_characters = []

for poudlard_character in characteristics_tab:
    for kaggle_character in characters_tab:
        if poudlard_character['Name'] == kaggle_character['Name']:
            poudlard_character.update(kaggle_character)
            poudlard_characters.append(poudlard_character)


index_id_characteristics = {int(character['Id']): 
                            (int(character['Courage']),
                             int(character['Ambition']),
                             int(character['Intelligence']),
                             int(character['Good'])) 
                            for character in poudlard_characters}


def distance (perso1, perso2, methode='euclidienne'):
    return sqrt((perso1['Courage'] - perso2['Courage']) ** 2 + (perso1['Ambition'] - perso2['Ambition']) ** 2 + (perso1['Intelligence'] - perso2['Intelligence']) ** 2 + (perso1['Good'] - perso2['Good']) ** 2)

def ajout_distances(index_id, perso_inconnu):
    for perso in index_id:
        perso['Distance'] = distance(perso_inconnu, perso)
    return index_id
    
profiles = ajout_distances(index_id_characteristics, profiles_student[0])

print(profiles)



'''k = 5
voisins = sorted(joueurs, key=lambda x: x['Distance'])
print(voisins[:k])
'''