#c
'''

Projet NSI : Choipeaux Magique

@author: Mayline, Yakin


'''
import csv
from math import sqrt

K = 5

with open("Characters.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    characters_tab = [{key : value.replace('\xa0', ' ') for key, value in element.items()} for element in reader]

with open("Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    characteristics_tab = [{key : value for key, value in element.items()} for element in reader]

profiles_student = [{'Name': 'student1', 'Courage' : 9, 'Ambition' : 2, 'Intelligence' : 8, 'Good' : 9},
                    {'Name': 'student2', 'Courage' : 6, 'Ambition' : 7, 'Intelligence' : 9, 'Good' : 7},
                    {'Name': 'student3', 'Courage' : 3, 'Ambition' : 8, 'Intelligence' : 6, 'Good' : 3},
                    {'Name': 'student4', 'Courage' : 2, 'Ambition' : 3, 'Intelligence' : 7, 'Good' : 8},
                    {'Name': 'student5', 'Courage' : 3, 'Ambition' : 4, 'Intelligence' : 8, 'Good' : 8}]

poudlard_characters = []

for poudlard_character in characteristics_tab:
    for kaggle_character in characters_tab:
        if poudlard_character['Name'] == kaggle_character['Name']:
            poudlard_character.update(kaggle_character)
            poudlard_characters.append(poudlard_character)



def distance (perso1, perso2):
    return sqrt((perso1['Courage'] - int(perso2['Courage'])) ** 2
              + (perso1['Ambition'] - int(perso2['Ambition'])) ** 2
              + (perso1['Intelligence'] - int(perso2['Intelligence'])) ** 2 
              + (perso1['Good'] - int(perso2['Good'])) ** 2)

def add_distances(table_poudlard, perso_inconnu):
    for perso in table_poudlard:
        perso['Distance'] = distance(perso_inconnu, perso)
    return table_poudlard

for student in profiles_student:

    distance_list = add_distances(poudlard_characters, student)
    voisins = sorted(distance_list, key=lambda x: x['Distance'])
    k_voisins = voisins[:K]

    houses_count = {'Gryffindor': 0, 'Hufflepuff':0, 'Ravenclaw' :0, 'Slytherin' :0 }
    for voisin in k_voisins:
        if voisin['House'] == 'Gryffindor':
            houses_count['Gryffindor'] += 1 
        elif voisin['House'] == 'Hufflepuff':
            houses_count['Hufflepuff'] += 1
        elif voisin['House'] == 'Ravenclaw':
            houses_count['Ravenclaw'] += 1
        elif voisin['House'] == 'Slytherin':
            houses_count['Slytherin'] += 1

        
    houses_list = list(houses_count.items())

    houses_list_sorted = sorted(houses_list, key=lambda x: x[1])


    print(houses_list_sorted.pop())

print()





