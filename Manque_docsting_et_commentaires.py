'''

Projet NSI : Choipeaux Magique

@author: Mayline, Yakin


'''
import csv
from math import sqrt

poudlard_characters = []
table_poudlard = []

with open("Characters.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    characters_tab = [{key : value.replace('\xa0', ' ') for key, value in element.items()} for element in reader]

with open("Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    characteristics_tab = [{key : value for key, value in element.items()} for element in reader]

for poudlard_character in characteristics_tab:
    for kaggle_character in characters_tab:
        if poudlard_character['Name'] == kaggle_character['Name']:
            poudlard_character.update(kaggle_character)
            poudlard_characters.append(poudlard_character)

student1 = {'Name': 'student1', 'Courage' : 9, 'Ambition' : 2, 'Intelligence' : 8, 'Good' : 9}
student2 = {'Name': 'student2', 'Courage' : 6, 'Ambition' : 7, 'Intelligence' : 9, 'Good' : 7}
student3 = {'Name': 'student3', 'Courage' : 3, 'Ambition' : 8, 'Intelligence' : 6, 'Good' : 3}
student4 = {'Name': 'student4', 'Courage' : 2, 'Ambition' : 3, 'Intelligence' : 7, 'Good' : 8}
student5 = {'Name': 'student5', 'Courage' : 3, 'Ambition' : 4, 'Intelligence' : 8, 'Good' : 8}
            

def choixpeau(perso, table, K):
    voisins = []
    for chara in table:
        distance = sqrt((int(chara['Courage']) - perso['Courage']) ** 2 + \
            (int(chara['Intelligence']) - perso['Intelligence']) ** 2 + \
            (int(chara['Ambition']) - perso['Ambition']) ** 2 + \
            (int(chara['Good']) - perso['Good']) ** 2)
        table_poudlard.append(distance)
        chara['Distance'] = distance
    table_poudlard.sort()

    for neigh in table:
        if len(voisins) < 5:
            if neigh['Distance'] in table_poudlard[:K]:
                voisins.append(neigh)
    k_voisins = voisins[:K]

    houses_count = {'Gryffindor': 0, 'Hufflepuff':0, 'Ravenclaw' :0, 'Slytherin' :0 }
    
    print(f'Les {K} plus proches voisins de {perso["Name"]} sont :')
    print(" ")
    for neighbour in k_voisins:
        print(f"{neighbour['Name']}, de la maison {neighbour['House']}")
        print(" ")
        if neighbour['House'] == 'Gryffindor':
            houses_count['Gryffindor'] += 1 
        elif neighbour['House'] == 'Hufflepuff':
            houses_count['Hufflepuff'] += 1
        elif neighbour['House'] == 'Ravenclaw':
            houses_count['Ravenclaw'] += 1
        elif neighbour['House'] == 'Slytherin':
            houses_count['Slytherin'] += 1
       
        houses_list = list(houses_count.items())
        houses_list_sorted = sorted(houses_list, key=lambda x: x[1])

    return(f'La maison choisie pour {perso["Name"]} est {houses_list_sorted[-1][0]} !')


print("Choisissez un profil à analyser, parmi les 5 prédéfinis : ")
profil = int(input(" Votre choix ? : "))
assert 1 <= profil <= 5, "Veuillez sélectionner un profil valide"
if profil == 1:
    K = int(input(f"Choisissez le nombre de voisins pour placer student{profil} : "))
    assert K > 0, "Veuillez sélectionner un nombre supérieur à 0"
    print(choixpeau(student1, poudlard_characters, K))
if profil == 2:
    K = int(input(f"Choisissez le nombre de voisins pour placer student{profil} : "))
    assert K > 0, "Veuillez sélectionner un nombre supérieur à 0"
    print(choixpeau(student2, poudlard_characters, K))
if profil == 3:
    K = int(input(f"Choisissez le nombre de voisins pour placer student{profil} : "))
    assert K > 0, "Veuillez sélecti5onner un nombre supérieur à 0"
    print(choixpeau(student3, poudlard_characters, K))
if profil == 4:
    K = int(input(f"Choisissez le nombre de voisins pour placer student{profil} : "))
    assert K > 0, "Veuillez sélectionner un nombre supérieur à 0"
    print(choixpeau(student4, poudlard_characters, K))
if profil == 5:
    K = int(input(f"Choisissez le nombre de voisins pour placer student{profil} : "))
    assert K > 0, "Veuillez sé1lectionner un nombre supérieur à 0"
    print(choixpeau(student5, poudlard_characters, K))